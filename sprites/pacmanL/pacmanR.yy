{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c02690e9-5da3-45ee-8d7a-9b00e5dbe9da","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c02690e9-5da3-45ee-8d7a-9b00e5dbe9da","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":{"name":"135e79c4-3dd8-4791-bbe1-457bed65bc1d","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"pacmanR","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"c02690e9-5da3-45ee-8d7a-9b00e5dbe9da","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"294220fb-8d87-402e-93d6-fdec551eee34","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"294220fb-8d87-402e-93d6-fdec551eee34","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":{"name":"135e79c4-3dd8-4791-bbe1-457bed65bc1d","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"pacmanR","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"294220fb-8d87-402e-93d6-fdec551eee34","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f998f41c-254a-4f50-98b7-1aa46b57ce7d","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f998f41c-254a-4f50-98b7-1aa46b57ce7d","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":{"name":"135e79c4-3dd8-4791-bbe1-457bed65bc1d","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"pacmanR","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"f998f41c-254a-4f50-98b7-1aa46b57ce7d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b0d5bd5-00ee-4c64-919b-f481518e12fe","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b0d5bd5-00ee-4c64-919b-f481518e12fe","path":"sprites/pacmanR/pacmanR.yy",},"LayerId":{"name":"135e79c4-3dd8-4791-bbe1-457bed65bc1d","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"pacmanR","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","name":"2b0d5bd5-00ee-4c64-919b-f481518e12fe","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"pacmanR","path":"sprites/pacmanR/pacmanR.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"77babeba-556e-46af-af41-00a1260c1089","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c02690e9-5da3-45ee-8d7a-9b00e5dbe9da","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3bc13264-310f-4915-9b94-8f4dda33f3f5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"294220fb-8d87-402e-93d6-fdec551eee34","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a2897ad-ee49-426f-8bbd-1a2a97bbdb03","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f998f41c-254a-4f50-98b7-1aa46b57ce7d","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5958336e-5abf-4627-a10b-d513f73b7313","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b0d5bd5-00ee-4c64-919b-f481518e12fe","path":"sprites/pacmanR/pacmanR.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"pacmanR","path":"sprites/pacmanR/pacmanR.yy",},
    "resourceVersion": "1.3",
    "name": "pacmanR",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"135e79c4-3dd8-4791-bbe1-457bed65bc1d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "pacmanR",
  "tags": [],
  "resourceType": "GMSprite",
}