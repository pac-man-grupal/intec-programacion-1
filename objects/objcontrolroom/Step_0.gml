/// Controlar todo el nivel

/// Activar los enemigos nuevamente

if !(audio_is_playing(spr_intro))
{
	instance_activate_object(ghost1)
}
if global.livs == 0
	{
	with (ghost1)
		{
		instance_destroy();
		}
	with (player)
		{
		instance_destroy();
		}
	with (objdot)
		{
		instance_destroy();
		}
	with (objpowerdot)
		{
		instance_destroy();
		}
	with (obj_bonus)
		{
		instance_destroy();
		}
	with (Object13)
		{
		visible = true;
		}
	}
	
	if instance_number(objdot) == 0 and instance_number(objpowerdot) == 0 and global.livs != 0
	{
		audio_stop_all();
		if room_next(room) != -1
		{
			room_goto_next();
		}
		
	}