/// @description dibujar puntos y vida
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(Fnt_text);
draw_text(32,16, "SCORE: "+string(global.puntos));
draw_text(room_width/2, 16, "LIVES: ");

for(var i = 0; i < global.livs; i += 1)
	{
	draw_sprite_stretched(pacmanR,2,room_width/2+string_width("LIVES: ")+(i * 33),16, 32,32);  
	}



if (audio_is_playing(spr_intro))
	{
	draw_set_color(c_yellow);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(fnt_ready);
	
	draw_text(room_width/2, room_height/2, "READY!");  
	}

if (global.bonus)
	{
	draw_sprite(Bono, 0, room_width - 64, 0);
	}