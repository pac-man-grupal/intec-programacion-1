/// @description Insert description here
// You can write your code in this editor
if global.hit == 1 or audio_is_playing(spr_intro)	
exit;
if keyboard_check(vk_right) && place_free(x+1,y) && place_snapped(16,16){
direction = 0;
speed = v;
}

if keyboard_check(vk_left) && place_free(x-1,y) && place_snapped(16,16){
direction = 180;
speed = v;
}

if keyboard_check(vk_up) && place_free(x,y-1) && place_snapped(16,16){
direction = 90;
speed = v;
}

if keyboard_check(vk_down) && place_free(x,y+1) && place_snapped(16,16){
direction = 270;
speed = v;
}

if speed>0{
image_speed = 1;	
}

switch (direction){
	case 0:
	sprite_index = pacmanR;
	break;
	
	case 90:
	sprite_index = pacmanU;
	break;
	
	case 180:
	sprite_index = pacmanL;
	break;
	
	case 270:
	sprite_index = pacmanD;
	break;
}

