{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "spr_nivelpass.wav",
  "duration": 5.209263,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_nivelpass",
  "tags": [],
  "resourceType": "GMSound",
}